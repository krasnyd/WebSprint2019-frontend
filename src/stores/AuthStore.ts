import {action, computed} from "mobx";
import Cookie from 'mobx-cookie';

class AuthStoreObject {
    SlackTokenCookie: Cookie = new Cookie("Token");
    SlackUsernameCookie: Cookie = new Cookie("Username");

    @computed get slackToken() {
        return this.SlackTokenCookie.value;
    };
    set slackToken(value: string) {
        this.SlackTokenCookie.set(value);
    };

    @computed get slackName() {
        return this.SlackUsernameCookie.value || "Petr Vaněk";
    };
    set slackName(value: string) {
        this.SlackUsernameCookie.set(value);
    };


    @computed get isLoggedIn(): boolean {
        return !!this.slackToken;
    }

    @action logout = (): void => {
        this.slackToken = "";
        this.slackName = "";
    }
}


const AuthStore = new AuthStoreObject();
export default AuthStore;
export {AuthStoreObject};
