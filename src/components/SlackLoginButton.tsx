import React, {Component} from 'react';
import {inject, observer} from "mobx-react";
import {AuthStoreObject} from "../stores/AuthStore";

interface SlackLoginButtonProps {
    AuthStore?: AuthStoreObject
}

@inject("AuthStore")
@observer
class SlackLoginButton extends Component<SlackLoginButtonProps> {
    render() {
        const clientId = "595124259783.785607650883";
        const scope = "identity.basic";

        return (
            <a className="loginButton"
                   href={`https://slack.com/oauth/authorize?scope=${scope}&client_id=${clientId}`}>
                <img
                     src="https://platform.slack-edge.com/img/sign_in_with_slack@2x.png"
                     alt="Login with Slack" />
            </a>
        );
    }
}


export default SlackLoginButton;