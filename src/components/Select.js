import React from "react";
import "./Select.css";

import Selection from "@simonwep/selection-js";

export const Select = (props) => {
    const selection = Selection.create({

        // Class for the selection-area
        class: 'selection',

        // All elements in this container can be selected
        selectables: ['.box-wrap > .selec'],

        // The container is also the boundary in this case
        boundaries: ['.box-wrap']
    }).on('start', ({inst, selected, oe}) => {

        // Remove class if the user isn't pressing the control key or ⌘ key
        if (!oe.ctrlKey && !oe.metaKey) {

            // Unselect all elements
            for (const el of selected) {
                el.classList.remove('selected');
                inst.removeFromSelection(el);
            }

            // Clear previous selection
            inst.clearSelection();
        }

    }).on('move', ({changed: {removed, added}}) => {

        // Add a custom class to the elements that where selected.
        for (const el of added) {
            el.classList.add('selected');
        }

        // Remove the class from elements that where removed
        // since the last selection
        for (const el of removed) {
            el.classList.remove('selected');
        }

    }).on('stop', ({inst}) => {
        inst.keepSelection();
    });
    return (
        <table>
            <tr>
                <td styles="text-align: center" colSpan="2">Pondělí</td>
                <td styles="text-align: center" colSpan="2">Úterý</td>
                <td styles="text-align: center" colSpan="2">Středa</td>
            </tr>
            <tr>
                <td>M</td>
                <td>V</td>
                <td>M</td>
                <td>V</td>
                <td>M</td>
                <td>V</td>
            </tr>
            <tr>
                <td>
                    <section className="box-wrap boxes green">
                        <div className="selec">10:30</div>
                        <div className="selec">11:00</div>
                        <div className="unselec">11:30</div>
                        <div className="selec">12:00</div>
                        <div className="selec">12:30</div>
                        <div className="unselec">13:00</div>
                        <div className="unselec">13:30</div>
                        <div className="selec">14:00</div>
                    </section>
                </td>

                <td>
                    <section className="box-wrap boxes green">
                        <div className="selec">10:30</div>
                        <div className="selec">11:00</div>
                        <div className="unselec">11:30</div>
                        <div className="selec">12:00</div>
                        <div className="selec">12:30</div>
                        <div className="unselec">13:00</div>
                        <div className="unselec">13:30</div>
                        <div className="selec">14:00</div>
                    </section>
                </td>
                <td>
                    <section className="box-wrap boxes green">
                        <div className="selec">10:30</div>
                        <div className="selec">11:00</div>
                        <div className="unselec">11:30</div>
                        <div className="selec">12:00</div>
                        <div className="selec">12:30</div>
                        <div className="unselec">13:00</div>
                        <div className="unselec">13:30</div>
                        <div className="selec">14:00</div>
                    </section>
                </td>
                <td>
                    <section className="box-wrap boxes green">
                        <div className="selec">10:30</div>
                        <div className="selec">11:00</div>
                        <div className="unselec">11:30</div>
                        <div className="selec">12:00</div>
                        <div className="selec">12:30</div>
                        <div className="unselec">13:00</div>
                        <div className="unselec">13:30</div>
                        <div className="selec">14:00</div>
                    </section>
                </td>
                <td>
                    <section className="box-wrap boxes green">
                        <div className="selec">10:30</div>
                        <div className="selec">11:00</div>
                        <div className="unselec">11:30</div>
                        <div className="selec">12:00</div>
                        <div className="selec">12:30</div>
                        <div className="unselec">13:00</div>
                        <div className="unselec">13:30</div>
                        <div className="selec">14:00</div>
                    </section>
                </td>
                <td>
                    <section className="box-wrap boxes green">
                        <div className="selec">10:30</div>
                        <div className="selec">11:00</div>
                        <div className="unselec">11:30</div>
                        <div className="selec">12:00</div>
                        <div className="selec">12:30</div>
                        <div className="unselec">13:00</div>
                        <div className="unselec">13:30</div>
                        <div className="selec">14:00</div>
                    </section>
                </td>

            </tr>

        </table>
    );
};