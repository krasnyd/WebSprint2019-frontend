import React from "react";

export const MyEvents = (props) => {
    return (
        <div>
            <div className={"myEvents"}>
                <div className={"event header"}>
                    <span className={"eventName"}>Název eventu</span>
                    <span className={"eventTime"}>Čas</span>
                    <span className={"eventHost"}>Pozvaní</span>
                    <span className={"eventRoom"}>Místnost</span>
                    <span className={"deleteEvent"}></span>
                </div>
                <div className={"event"}>
                    <span className={"eventName"}>Schůzka s zájemci o práci</span>
                    <span className={"eventTime"}>18.09. 12:00 - 15:00</span>
                    <span className={"eventHost"}>Externí</span>
                    <span className={"eventRoom"}>Malá místnost</span>
                    <span className={"deleteEvent"}>X</span>
                </div>
                <div className={"event"}>
                    <span className={"eventName"}>Kurz zamykání obrazovek</span>
                    <span className={"eventTime"}>18.09 11:30 - 13:00</span>
                    <span className={"eventHost"}>Honza Steinbach</span>
                    <span className={"eventRoom"}>Velká místnost</span>
                    <span className={"deleteEvent"}>X</span>
                </div>
                <div className={"event"}>
                    <span className={"eventName"}>Počítáme nekonečné trpaslíky</span>
                    <span className={"eventTime"}>1.12. 12:00 - 15:00</span>
                    <span className={"eventHost"}>Pavol Hejný</span>
                    <span className={"eventRoom"}>Velká místnost</span>
                    <span className={"deleteEvent"}>X</span>
                </div>
                <div className={"event"}>
                    <span className={"eventName"}>Schůzka nad Vodíkem</span>
                    <span className={"eventTime"}>29.10. 8:00 - 9:30</span>
                    <span className={"eventHost"}>Honza Kučera</span>
                    <span className={"eventRoom"}>Malá místnost</span>
                    <span className={"deleteEvent"}>X</span>
                </div>
                <div className={"event"}>
                    <span className={"eventName"}>Vykrádání codebase <a href="https://socialreaders.gitlab.io">SocialReaders</a></span>
                    <span className={"eventTime"}>1.12. 16:00 - 19:00</span>
                    <span className={"eventHost"}>Patrik Vácal, Petr Vaněk, David Krásný</span>
                    <span className={"eventRoom"}>Malá místnost</span>
                    <span className={"deleteEvent"}>X</span>
                </div>
            </div>
        </div>
    )
};