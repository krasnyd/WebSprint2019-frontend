import React from "react";

import "./Input.css"


const Input = (props) => {
    let inputElement = null;
    let prefix = null;

    if (props.prefix) {
        prefix = (
            <td className="InputPrefix">{props.prefix}</td>
        );
    }

    switch(props.elementType) {
        case "input":
            inputElement = <input
                className='InputElement'
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
            break;
        case "textarea":
            inputElement = <textarea
                className='InputElement'
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
            break;
        default:
            inputElement = <input
                className='InputElement'
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
    }

    return (
        <div>
            <label className="Label">{props.label}</label>
            <table className="InputTable">
                <tbody>
                    <tr>
                        {prefix}
                        <td>{inputElement}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
};

export default Input;
