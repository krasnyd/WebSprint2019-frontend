import React from "react";
import AuthStore from "../stores/AuthStore";
import {withRouter} from "react-router";


export const MainPageLoggedIn = withRouter((props) => (
    <div className="container">
        <header>
            <div className="user" onClick={AuthStore.logout}>  {/* TODO Remove this type of LOGOUT */}
                <svg className="userIcon" viewBox="0 0 24 24">
                    <path
                        d="M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z"/>
                </svg>
                <span className="userName">{AuthStore.slackName}</span>
            </div>
            <div className="tabs">
                <div className="tab" onClick={() => props.history.push("/myEvents")}>>
                    <svg className="tabIcon" viewBox="0 0 24 24">
                        <path
                            d="M7,5H21V7H7V5M7,13V11H21V13H7M4,4.5A1.5,1.5 0 0,1 5.5,6A1.5,1.5 0 0,1 4,7.5A1.5,1.5 0 0,1 2.5,6A1.5,1.5 0 0,1 4,4.5M4,10.5A1.5,1.5 0 0,1 5.5,12A1.5,1.5 0 0,1 4,13.5A1.5,1.5 0 0,1 2.5,12A1.5,1.5 0 0,1 4,10.5M7,19V17H21V19H7M4,16.5A1.5,1.5 0 0,1 5.5,18A1.5,1.5 0 0,1 4,19.5A1.5,1.5 0 0,1 2.5,18A1.5,1.5 0 0,1 4,16.5Z"/>
                    </svg>
                    <span className="tabName">Moje eventy</span>
                </div>
                <div className="tab" onClick={() => props.history.push("/largeRoom")}>
                    <svg className="tabIcon" viewBox="0 0 24 24">
                        <path
                            d="M9,10V12H7V10H9M13,10V12H11V10H13M17,10V12H15V10H17M19,3A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5C3.89,21 3,20.1 3,19V5A2,2 0 0,1 5,3H6V1H8V3H16V1H18V3H19M19,19V8H5V19H19M9,14V16H7V14H9M13,14V16H11V14H13M17,14V16H15V14H17Z"/>
                    </svg>
                    <div className="tabName">Velká místnost</div>
                </div>
                <div className="tab" onClick={() => props.history.push("/smallRoom")}>
                    <svg className="tabIcon" viewBox="0 0 24 24">
                        <path
                            d="M9,10V12H7V10H9M13,10V12H11V10H13M17,10V12H15V10H17M19,3A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5C3.89,21 3,20.1 3,19V5A2,2 0 0,1 5,3H6V1H8V3H16V1H18V3H19M19,19V8H5V19H19M9,14V16H7V14H9M13,14V16H11V14H13M17,14V16H15V14H17Z"/>
                    </svg>
                    <div className="tabName">Malá místnost</div>
                </div>
                <div className="tab" onClick={() => props.history.push("/addEvent")}>
                    <svg className="tabIcon" viewBox="0 0 24 24">
                        <path
                            d="M19 19V8H5V19H19M16 1H18V3H19C20.11 3 21 3.9 21 5V19C21 20.11 20.11 21 19 21H5C3.89 21 3 20.1 3 19V5C3 3.89 3.89 3 5 3H6V1H8V3H16V1M11 9.5H13V12.5H16V14.5H13V17.5H11V14.5H8V12.5H11V9.5Z"/>
                    </svg>
                    <div className="tabName">Přidat event</div>
                </div>
            </div>
        </header>
        <div className="content">
            {props.children}
        </div>
    </div>
));