// import React, {Component} from "react";
// import {Redirect} from "react-router";
// import {inject, observer} from "mobx-react";
// import {AuthStoreObject} from "../stores/AuthStore";
//
//
// interface RedirectIfLoggedOutProps {
//     AuthStore?: AuthStoreObject
//     target: string
// }
//
// @inject("AuthStore")
// @observer
// class RedirectIfLoggedOut extends Component<RedirectIfLoggedOutProps> {
//     render()  {
//         return this.props.AuthStore.isLoggedIn ? null : <Redirect to={this.props.target} />;
//     };
// }
//
//
// export default RedirectIfLoggedOut;