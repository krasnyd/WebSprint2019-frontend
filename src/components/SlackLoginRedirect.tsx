import React, {Component} from 'react';
import {inject, observer} from "mobx-react";
import {AuthStoreObject} from "../stores/AuthStore";
import {withRouter} from "react-router";
import parseQueryString from "query-string";
import getAccessKey from "../API/slack/getAccessKey";

interface SlackLoginRedirectProps {
    AuthStore?: AuthStoreObject
    location?
    history?
}

@inject("AuthStore")
@observer
class SlackLoginRedirect extends Component<SlackLoginRedirectProps> {
    handleSuccess = async (accessToken, { response, state }) => {
        console.log('Successfully authorized: ', accessToken, response, state);
        // await setProfileFromDropbox(accessToken);
        // await redirect(state.from);
    };

    handleError = error => {
        console.error('An error occurred');
        console.error(error.message);
    };

    componentWillMount(): void {
        const queryParsed = parseQueryString.parse(this.props.location.search);
        const code: string = (queryParsed.code || "").toString();

        const clientId = "595124259783.785607650883";
        const clientSecret = "2818198f29ae65d777dc72357ee47e88";
        getAccessKey(clientId, clientSecret, code)
            .then(data => {
                console.log("getAccessKey, data: ", data);
                this.props.AuthStore.slackName = data.data.user.name;
                this.props.AuthStore.slackToken = data.data.access_token;
                this.props.history.push("/");
            })
    }

    render() {

        return (
            <div className="SlackLoginRedirect">
                Probíhá přihlašování...
            </div>
        );
    }
}


export default withRouter(SlackLoginRedirect);