import React, {Component} from "react";
import "./AreYouSureDialog.sass";

import Overlay from "../Overlay/Overlay";


interface AreYouSureDialogProps {
    prompt: string

    yesText?: String
    noText?: String
    yesAction: any
    noAction: any
}


class AreYouSureDialog extends Component<AreYouSureDialogProps> {
    render() {
        return (
            <Overlay closeCallback={this.props.noAction}>
                <div className="AreYouSureDialog">
                    <div className="Prompt">{this.props.prompt}</div>
                    <div className="NoButton">
                        <button className="Button" onClick={this.props.yesAction}>
                            {this.props.yesText || "ANO"}
                        </button>
                    </div>
                    <div className="YesButton">
                        <button className="Button Danger" onClick={this.props.noAction}>
                            {this.props.noText || "NE"}
                        </button>
                    </div>
                </div>
            </Overlay>
        )
    }
}

export default AreYouSureDialog;
