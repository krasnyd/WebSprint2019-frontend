import React, {Component} from "react";
import "./Overlay.sass";


interface OverlayProps {
    closeCallback
}

class Overlay extends Component<OverlayProps> {
    render() {
        return (
            <div className="OverlayContainer">
                <div className="background" onClick={() => this.props.closeCallback()}/>
                <div className="content">
                   {this.props.children}
                </div>
            </div>
        )
    }
}

export default Overlay;