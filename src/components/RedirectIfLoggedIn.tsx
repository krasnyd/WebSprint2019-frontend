// import React, {Component} from "react";
// import {Redirect} from "react-router";
// import {inject, observer} from "mobx-react";
// import {AuthStoreObject} from "../stores/AuthStore";
//
//
// interface RedirectIfLoggedInProps {
//     AuthStore?: AuthStoreObject
//     target: string
// }
//
// @inject("AuthStore")
// @observer
// class RedirectIfLoggedIn extends Component<RedirectIfLoggedInProps> {
//     render()  {
//         return this.props.AuthStore.isLoggedIn ? <Redirect to={this.props.target} /> : null;
//     };
// }
//
//
// export default RedirectIfLoggedIn;