import React, {Component} from 'react';
import './App.sass';
import {BrowserRouter, Route} from "react-router-dom";
import Logout from "./containers/Logout";
import InfoMessage from "./containers/InfoMessage/InfoMessage";
import {AuthStoreObject} from "./stores/AuthStore";
import {inject, observer} from "mobx-react";
import SlackLoginRedirect from "./components/SlackLoginRedirect";
import {MainPageLoggedIn} from "./components/MainPageLoggedIn";
import SlackLoginButton from "./components/SlackLoginButton";
import {roomImage} from "./assets";
import {SmallRoom} from "./containers/SmallRoom";
import {LargeRoom} from "./containers/LargeRoom";
import AddEvent from "./containers/AddEvent";
import {MyEvents} from "./components/MyEvents";


interface AppProps {
    AuthStore?: AuthStoreObject
}

@inject("AuthStore")
@observer
class App extends Component<AppProps> {
    state = {};

    render() {
        let menu = null;
        if (this.props.AuthStore.isLoggedIn) {
            menu = (
                <MainPageLoggedIn>
                    <Route path="/smallRoom" exact component={SmallRoom}/>
                    <Route path="/largeRoom" exact component={LargeRoom}/>
                    <Route path="/addEvent" exact component={AddEvent}/>
                    <Route path="/myEvents" exact component={MyEvents}/>
                </MainPageLoggedIn>
            );
        } else {
            menu = (
                <div className="loginContainer">
                    <img src={roomImage} className="backgroundImage"/>
                    <div className="headline">
                        <h1>TCBC</h1>
                        <h2>TechHeaven conference room booking client</h2>
                    </div>
                    <SlackLoginButton/>
                </div>
            )
        }

        return (
            <BrowserRouter>
                {/* Content of BrowserRouter must be in div to not throw warnings *!/*/}
                <InfoMessage />

                {menu}

                <div className="AppContent">
                {/*        /!*<Route path="/" exact component={MainPage}/>*!/*/}
                    <Route path="/logout" exact component={Logout}/>
                    <Route path="/auth/slack/redirect" exact component={SlackLoginRedirect}/>
                </div>
            </BrowserRouter>
        );
    }
}


export default App;