import React, {Component} from "react";
import {inject, observer} from "mobx-react";
import {withRouter} from "react-router";
import {AuthStoreObject} from "../stores/AuthStore";


interface LogoutProps {
    AuthStore?: AuthStoreObject
    history?

    LogoutRedirect?: string
}

@inject("AuthStore")
@observer
class Logout extends Component<LogoutProps> {
    componentWillMount(): void {
        console.log("LOGOUT");

        this.props.AuthStore.logout();

        this.props.history.push("/");
    }

    render() {
        return (
            <div>LOGOUT</div>
        )
    };

    componentWillUnmount(): void {

    }
}


export default withRouter(Logout);
