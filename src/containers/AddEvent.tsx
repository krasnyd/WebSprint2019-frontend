import React, {Component} from "react";
import {withRouter} from "react-router";

import {observer} from "mobx-react";
import {Select} from '../components/Select';
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'
import Input from "../components/Input/Input";
import Button from "../Button/Button";


interface AddBookProps {
    history?
}

@observer
class AddEvent extends Component<AddBookProps> {
    state = {
        formElements: {
            name: {
                promptText: "Název: ",
                elementType: "input",
                elementConfig: {
                    type: "text",
                    placeholder: "název",
                },
                value: "",
                validation: {
                    required: true,
                },
                valid: false,
                touched: false,
            },
            description: {
                promptText: "Popis: ",
                elementType: "input",
                elementConfig: {
                    type: "text",
                    placeholder: "popis",
                },
                value: "",
                validation: {
                    required: true,
                },
                valid: false,
                touched: false,
            },
        },
        // selectedThumbnailUrl: defaultBookThumbnail,

        error: "",
        loading: false,
        addingBook: null,
        bookSuggestionsVisible: false,
    };

    componentWillMount() {
        // this.setState({
        //     formElements: {
        //         ...this.state.formElements,
        //         phone: {
        //             ...this.state.formElements.phone,
        //             value: this.props.AuthStore.loginFlow_phone,
        //         }
        //     },
        //     error: null,
        // });
    }

    // Can be called only once before each update!!!
    private inputChangedHandler = (value, inputIdentifier) => {
        const updatedControls = {
            ...this.state.formElements,
        };
        const updatedFormElement = {
            ...updatedControls[inputIdentifier],
        };

        updatedFormElement.value = value;
        updatedFormElement.valid = AddEvent.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;

        updatedControls[inputIdentifier] = updatedFormElement;
        this.setState({
            formElements: updatedControls,
        });
        if (this.state.error === "InvalidField" || this.state.error === "BookAlreadyAdded") {
            this.setState({error: ""});
        }

        if (inputIdentifier === "name" && updatedFormElement.valid)this.setState({bookSuggestionsVisible: true});
        else                                                       this.setState({bookSuggestionsVisible: false});

        // If name is changed, set default thumbnail
        // if (inputIdentifier === "name") this.setState({selectedThumbnailUrl: defaultBookThumbnail});

        // console.log(updatedControls);
        // console.log(this.state);
    };
    //
    // private submitHandler = (event) => {
    //     event.preventDefault();
    //     console.log("Submit clicked");
    //
    //     this.setState({loading: true});
    //
    //     // Check if all user fields are valid before submitting.
    //     let isInvalid = false;
    //     const formElements = this.state.formElements;
    //     for (const formInput in formElements) {
    //         if (!formElements.hasOwnProperty(formInput)) continue;
    //         formElements[formInput].touched = true;
    //         if (!formElements[formInput].valid) {
    //             console.log("Not all user fields are valid, not submitting.");
    //             this.setState({error: "InvalidField"});
    //             this.props.history.push({
    //                 search: `?message=Některé zadané informace nejsou validní!&message_type=warning`,
    //             });
    //             isInvalid = true;
    //         }
    //     }
    //     this.setState({formElements});
    //     if (isInvalid) return;
    //
    //     const book: UserBook = new UserBook();
    //     book.name = this.state.formElements.name.value;
    //     book.authors = [this.state.formElements.author.value];
    //     book.isbn = this.state.formElements.isbn.value;
    //     book.categories = this.state.formElements.categories.value;
    //     book.userStatus = this.state.formElements.userState.value as BOOK_USER_STATES;
    //     book.userOwn = this.state.formElements.ownBook.value;
    //     book.landable = this.state.formElements.landable.value;
    //     book.thumbnail = this.state.selectedThumbnailUrl;
    //
    //     this.setState({addingBook: book});
    //
    //     addBook(book).then(data => {
    //         if (data.success) {
    //             const message = `Přidal jsem knihu ${book.name} do Vaší knihovny`;
    //             this.props.CacheStore.forceFetchBooks();
    //             this.props.history.push({
    //                 pathname: "/profile",
    //                 search: `?message=${message}&message_type=success`,
    //             });
    //         } else {
    //             this.setState({
    //                 addingBook: null,
    //                 error: data.error.data.code,
    //             });
    //             if (data.error.data.code === "BookAlreadyAdded") {
    //                 this.props.history.push({
    //                     search: `?message=Tuto knihu již máte ve svojí knihovně!&message_type=warning`
    //                 });
    //             }
    //         }
    //     });
    // };

    // TODO edit to match what is needed (+ isIsbn)
    static checkValidity = (value, rules) => {
        // console.log(`Checking validity for "${value}" with rules:`);
        // console.log(rules);

        let isValid = true;

        if (rules.required) {
            if (typeof value === "string") isValid = isValid && (value.trim() !== "");
            if (typeof value === "boolean") isValid = true;
            else isValid = isValid && !!value;
        } else if (!value) {
            return true;
        }

        if (rules.minLength) {
            isValid = isValid && (value.length >= rules.minLength);
        }

        if (rules.requiredLength) {
            isValid = isValid && (value.length === rules.requiredLength);
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = isValid && pattern.test(value);
        }

        if (rules.isNumber) {
            const pattern = /^\d+$/;
            isValid = isValid && pattern.test(value);
        }

        // if (rules.isIsbn) {
        //     isValid = isValid && (isValidIsbn10(value) || isValidIsbn13(value));
        // }

        return isValid;
    };

    // private onSearchBookSelect = (book: Book) => {
    //     console.log("User selected book from search: ", book);
    //
    //     const updatedControls = {
    //         ...this.state.formElements,
    //     };
    //
    //     updatedControls["name"].value = book.name || "";
    //     updatedControls["name"].valid = true;
    //     updatedControls["name"].touched = true;
    //
    //     updatedControls["author"].value = (book.authors || [""])[0] || "";
    //     updatedControls["author"].valid = true;
    //     updatedControls["author"].touched = true;
    //
    //     updatedControls["isbn"].value = book.isbn || "";
    //     updatedControls["isbn"].valid = true;
    //     updatedControls["isbn"].touched = true;
    //
    //     updatedControls["categories"].value = book.categories || [];
    //     updatedControls["categories"].valid = true;
    //     updatedControls["categories"].touched = true;
    //
    //     this.setState({
    //         formElements: updatedControls,
    //         bookSuggestionsVisible: false,
    //         selectedThumbnailUrl: book.thumbnail,
    //     });
    //
    //     if (this.state.error === "InvalidField" || this.state.error === "BookAlreadyAdded") {
    //         this.setState({
    //             error: "",
    //         })
    //     }
    // };


    render() {
        const controlsElementArray = [];
        for (let key in this.state.formElements) {
            if (this.state.formElements[key].isVisible) {
                if (! this.state.formElements[key].isVisible()) {
                    continue;
                }
            }
            controlsElementArray.push({
                id: key,
                config: this.state.formElements[key],
            });
        }

        let formElements: any[] = controlsElementArray.map(formElement => {
            let classes = "";
            if (!formElement.config.valid && formElement.config.touched) classes += "InvalidInput";
            switch(formElement.config.elementType) {
                case "input":
                    return (
                        <div>
                            <Input
                                key={formElement.id}
                                label={formElement.config.promptText}
                                elementType={formElement.config.elementType}
                                elementConfig={{...formElement.config.elementConfig, className: classes}}
                                value={formElement.config.value || ""}
                                changed={(event) => this.inputChangedHandler(event.target.value, formElement.id)}/>
                            {formElement.config.additionalComponents ? formElement.config.additionalComponents() : null}
                        </div>
                    );
                case "checkbox":
                    return (
                        <div>
                            <h4>{formElement.config.promptText}</h4>
                            <input className={classes}
                                   type="checkbox" defaultChecked={formElement.config.value}
                                   onChange={(e) => this.inputChangedHandler(e.target.checked, formElement.id)}/>
                            {formElement.config.additionalComponents ? formElement.config.additionalComponents() : null}
                        </div>
                    );
                case "tag":
                    return (
                        <div>
                            <h4>{formElement.config.promptText}</h4>
                            <TagsInput className={classes}
                                       type="checkbox" value={formElement.config.value}
                                       onChange={(tags) => this.inputChangedHandler(tags, formElement.id)}/>
                            {formElement.config.additionalComponents ? formElement.config.additionalComponents() : null}
                        </div>
                    );
                case "select":
                    return (
                        <div>
                            <h4>{formElement.config.promptText}</h4>
                            <Select className={classes}
                                    options={formElement.config.elementConfig.elements}
                                    value={formElement.config.elementConfig.elements.filter(element =>
                                        element.value === formElement.config.value)}
                                    onChange={(selected) => this.inputChangedHandler(selected.value, formElement.id)}/>
                            {formElement.config.additionalComponents ? formElement.config.additionalComponents() : null}
                        </div>
                    );
                default:
                    return <h4>Neznámý typ vstupního pole!</h4>;
            }
        });

        // let addingBookElement = null;
        // if (this.state.addingBook) {
        //     addingBookElement = (
        //         <Overlay closeCallback={() => this.setState({addingBook: null})}>
        //             Přidávám knížku "{this.state.addingBook.name}" do Vaší knihovny.
        //         </Overlay>
        //     );
        // }

        let form = (
            <form>
                {/*<h1>Přidej si knížku</h1>*/}

                {formElements}
                {this.getErrorMessage()}
                {/*{addingBookElement}*/}
                <Button buttonType="Success">Přidat</Button>
                <Select/>
            </form>
        );

        return (
            <div className="AddBook">
                {form}
            </div>
        )
    }

    // TODO
    getErrorMessage = () => {
        let errorMessage = null;

        switch (this.state.error) {
            case "":
                // No error
                break;
            case "InvalidField":
                errorMessage = "Některé zadané informace nejsou validní. Zkus to prosím opravit.";
                break;
            case "BookAlreadyAdded":
                // TODO show name and link to book that is already added.
                errorMessage = "Tuto knížku jste již přidali.";
                break;
            default:
                console.error("Unknown error: ", this.state.error);
                errorMessage = (
                    <div>
                        Stala se neznámá chyba! Prosíme kontaktujte správce:
                        <a href="mailto:patrik.vacal@gmail.com">patrik.vacal@gmail.com</a>
                    </div>
                );
        }

        return (
            <div className="ErrorMessage">
                {errorMessage}
            </div>
        );
    };
}


export default withRouter(AddEvent);