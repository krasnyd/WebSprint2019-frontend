import React, {Component} from "react";
import "./MainPage.css"
import {observer} from "mobx-react";


// @inject("AuthStore")
@observer
class MainPage extends Component {
    render() {
        let loggedInText = "";
        // if (this.props.AuthStore.isLoggedIn) {
        //     loggedInText = (
        //         <div> logged in </div>
        //     );
        // }

        return (
            <div className="MainMenu">
                <h1>Hlavní stránka</h1>
                {loggedInText}
            </div>
        );
    };
}


export default MainPage;