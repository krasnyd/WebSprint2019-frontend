import React, {Component} from "react";
import {withRouter} from "react-router-dom";
import parseQueryString from "query-string";

import "./InfoMessage.sass";


interface InfoMessageProps {
    location?: any
    history?: any
}

class InfoMessage extends Component<InfoMessageProps> {
    close = () => {
        this.props.history.push({
            search: "",
        });
    };

    render() {
        const queryParsed = parseQueryString.parse(this.props.location.search);
        const message = queryParsed.message;
        const type = queryParsed.message_type;
        if (!message) return null;

        let className: string = "";
        switch(type) {
            case "warning":
                className = "InfoMessageWarning";
                break;
            case "success":
                className = "InfoMessageSuccess";
                break;
        }

        return (
            <div className={"InfoMessage " + className}>
                <div className="InfoMessageMessage">{message}</div>
                <div className="InfoMessageClose" onClick={this.close}>X</div>
            </div>
        )
    };
}



export default withRouter(InfoMessage);