import React from "react";

export const LargeRoom = (props) => (
    <iframe className="calendarIframe"
            src="https://calendar.google.com/calendar/embed?src=q20upram0i68lk9biob40q2enk%40group.calendar.google.com&ctz=Europe%2FPrague"
            frameBorder="0" scrolling="no"></iframe>
);