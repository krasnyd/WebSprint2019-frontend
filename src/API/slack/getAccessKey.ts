import axios from "axios/index";

const url = "https://slack.com/api/oauth.access";


const getAccessKey = async (clientId: string, clientSecret: string, code: string): Promise<any> => {
    console.log(`Sending request to ${url}`);

    const params = {
        client_id: clientId,
        client_secret: clientSecret,
        code,
    };

    return await getPromise(params, {})
        .then((response) => {
            console.log(`Response from ${url}, success: `, response);
            return {
                success: true,
                data: response.data,
            };
        })
        .catch(async (error) => {
            console.error(`Response from ${url}, error:`, error);
            return {
                success: false,
                error: {
                    status: error.response.status,
                    data: error.response.data,
                }
            };
        });
};
const getPromise = async (params, headers) => {
    return axios.get(url,{
        headers,
        params,
    });
};


export default getAccessKey;