export class ValueInObject {
    constructor(val) {
        this.val = val;
    }

    get() {
        // console.log("ValueInObject - get: ", this.val);
        return this.val;
    }
    set(val) {
        // console.log("ValueInObject - set: ", val);
        this.val = val;
    }
}